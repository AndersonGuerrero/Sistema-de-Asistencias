from django.contrib import admin
from models import Persona, Imagen

admin.site.register(Persona)
admin.site.register(Imagen)
