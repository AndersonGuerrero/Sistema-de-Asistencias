from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required


from modules.online.views import Index
from base.views import Login, lx_logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', Login.as_view()),
    url(r'^logout/', lx_logout, name='logout'),
    url(r'^$', login_required(Index.as_view()), name='index_web'),
    url('^online/', include('modules.online.urls')),
]
