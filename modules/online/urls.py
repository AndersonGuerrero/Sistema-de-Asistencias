from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from views import (NuevaPersona, IndexPersona, Index,
                   RegistrarAsistencias, Reportes,
                   ListaPersonaAjax, ActualizarPersona,
                   registrar_asistencias_ajax)

urlpatterns = [
    url(r'^$', login_required(Index.as_view()), name='index-online'),
    url(r'^persona/nueva/$', login_required(NuevaPersona.as_view()),
        name='nueva-persona'),
    url(r'^persona/actualizar/(?P<id>[0-9]+)/$',
        login_required(ActualizarPersona.as_view()),
        name='actualizar-persona'),
    url(r'^persona/lista/$', login_required(IndexPersona.as_view()),
        name='index-persona'),
    url(r'^persona/lista/ajax/$', login_required(ListaPersonaAjax.as_view()),
        name='lista-persona-ajax'),
    url(r'^persona/actualizar/$', login_required(IndexPersona.as_view()),
        name='actualizar-persona'),
    url(r'^asistencias/registrar/(?P<tipo>[^/]+)/$',
        login_required(RegistrarAsistencias.as_view()),
        name='registrar-asistencias'),
    url(r'^reportes/(?P<tipo>[^/]+)/$', login_required(Reportes.as_view()),
        name='reportes'),
    url(r'^registrar_asistencias_ajax/$',
        login_required(registrar_asistencias_ajax),
        name='registrar_asistencias_ajax')
]
